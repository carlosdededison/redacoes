Tema: Caminhos para a consolidação da ética no Brasil

Ética, Educação e Exemplo
=========================

O conceito de ética está intimamente relacionado à cultura de um povo e seus
padrões de comportamento, de modo a sinalizar a cada pessoa aquilo que é certo
ou errado. Mesmo assim, ainda são comuns ações que destoam desses princípios
morais, desrespeitando e prejudicando a vida em sociedade. Por isso, é
necessário que encontremos soluções para acabar ou, pelo menos, amenizar os
efeitos desses desvios, de modo a buscar uma sociedade cada vez mais unida e
respeitosa.

A família é o primeiro e mais importante ambiente de aprendizado do cidadão, e
é nela que a convivência com outras pessoas tem início. No entanto, a criança
pode apresentar desde seus primeiros anos um comportamento considerado nocivo,
principalmente por causa do exemplo ou da falta deste. Também não são raras
situações nas quais uma atitude da família seria necessária em relação ao
comportamento do filho, mas não é tomada, por falta de instrução dos pais ou
por pura negligência.

Para a grande maioria das crianças, a correção da maioria dos possíveis desvios
se dá na escola que, por ser um ambiente de convivência mais diversificada,
também os torna mais evidentes. Além disso, exemplos transmitidos pelos
professores aos alunos também são capazes de influenciar positivamente a sua
tomada de decisões. Por isso, uma educação de qualidade desde as séries
iniciais constitui-se como um dos pilares fundamentais para a construção de um
cidadão consciente e honesto.

Na vida adulta, os padrões que compõem a ética já estão estabelecidos. No
entanto, o indivíduo ainda pode se corromper, devido a, muitas vezes, um
sentimento de impunidade, infelizmente cada vez mais presente na sociedade.
Pequenos desvios como faltar ao trabalho, desrespeitar filas, comprar mídias e
produtos pirateados, ultrapassar limites de velocidade no trânsito, entre
muitos outros, foram sendo desconsiderados à medida que seus praticantes foram
ficando impunes, e quem busca fazer o certo acaba prejudicado.

Não há um só caminho para fortalecer a cultura de ética no Brasil: ela faz
parte das qualidades adquiridas e desenvolvidas ao longo do crescimento do
indivíduo e da sociedade como um todo. Assim como outros modelos de conduta,
sua forma de transmissão mais expressiva é através do exemplo, e este sempre se
fortalece à medida que mais pessoas os seguem. Por isso, a educação dentro das
famílias e escolas e o combate à impunidade, mesmo nos menores delitos, são
essenciais para a construção de um país mais honesto e ético.
