# Preconceito linguístico: a esteriotipificação da comunicação

A variação linguística é o fenômeno que descreve as variações dentro de um
mesmo idioma, de acordo com indicadores como localização geográfica, nível de
educação, renda, entre outros. Das diferentes formas de se falar e escrever,
surge o preconceito linguístico: a forma de discriminação relacionada à
comunicação, classificando-a com base em estereótipos. Assim como é ideal
manter a língua portuguesa padronizada, é inaceitável que aqueles que não têm
acesso a ela sejam marginalizados.

Uma das principais causas da enorme variação linguística no Brasil é a
heterogeneidade de seu povo e território, devido à extensão e forma com que foi
colonizado, que, combinados, propiciaram a diversidade que temos hoje. Dessa
forma, tornou-se cada vez mais necessária a adoção de uma norma padrão, de modo
a propiciar um conjunto de regras estável para o compartilhamento de
informações.  Infelizmente, nem todas as pessoas tiveram acesso à educação
necessária para conhecer essas normas e segui-las.

Além disso, as variações e o preconceito linguístico se tornam cada vez mais
relevantes, à medida que se tornam cada vez mais evidentes os contrastes
causados por eles. Desde o uso quase exclusivo da norma culta em programas de
televisão até a exploração de seus desvios para fins humorísticos, os meios de
comunicação de massa também exercem influênciaa significativa acerca do tema. Isso acaba gerando
uma espécie de hierarquia, na qual as pessoas cuja linguagem mais se assemelha
à norma culta são considerados mais educados e capazes.

Não existe solução para acabar com o preconceito, mas existem formas de
freá-lo: já existem campanhas a favor do respeito às diferenças, que às vezes
são inclusive ignoradas pelos próprios veículos que as publicam. O verdadeiro
problema nesse caso é o acesso à educação básica, que pode e deve ser cada vez
mais facilitado tanto para crianças quanto para adultos. É função do poder público atuar em duas frentes: tanto no acesso a educação de qualidade, quanto no combate à discriminação que esta proporciona.
