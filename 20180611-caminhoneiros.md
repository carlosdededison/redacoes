(40 linhas, modelo UNISINOS)

As Redes Sociais e sua Influência na Greve dos Caminhoneiros
============================================================

Iniciada no dia 21 de maio, a greve organizada por caminhoneiros,
em sua maioria autônomos, de todo o País durou aproximadamente
duas semanas, afetando principalmente o transporte de alimentos e
combustível. Além de escancarar a dependência do brasileiro, 
tanto do combustível para seus carros, quanto do transporte 
rodoviário, considerado tão ineficiente frente às alternativas 
ferroviária e hidroviária, por exemplo, a mobilização mostrou a
importância e eficiência das redes sociais para a comunicação
entre as pessoas.

Além da disseminação de notícias acerca do movimento, a Internet --
com destaque para o aplicativo *Whatsapp* -- teve papel fundamental
na organização das manifestações, já que a praticidade e velocidade
com que as mensagens são enviadas facilitou a comunicação entre
caminhoneiros e lideranças do setor. De mais a mais, o apoio de 
parte da população à paralisação logo se tornou evidente. Por
exemplo, a greve recebeu cerca de 3,4 milhões de menções na rede
social *Twitter*. 

Entre as reivindicações dos motoristas, destacam-se a redução
do preço do *diesel* e o aumento do valor dos fretes. O valor dos combustíveis, que é calculado com base no mercado externo,
vem aumentando em ritmo acelerado, e a remuneração dos
trabalhadores autônomos não acompanha essa mudança, reduzindo
cada vez mais o lucro e a capacidade de subsistência dos
caminhoneiros. Além disso, muitos deles são privados de
direitos trabalhistas, por não possuírem vínculos empregatícios com
nenhuma empresa.

Apesar de contar com o apoio de parte da população, inclusive de
muitos agricultores, também insatisfeitos com o custo do
combustível, a greve trouxe ao agronegócio um prejuízo estimado em 
14 bilhões de reais. Essas despesas são calculadas com base na
quantidade de animais mortos por falta de alimentação e na
produção de leite, carne e outros alimentos que não puderam ser
transportados, que teve de ser descartada.

Ainda que tenha reduzido o preço do *diesel*, as manifestações não
trouxeram consequências significativamente positivas para a
população em geral. O aumento no custo do frete, refletido no
valor dos produtos para o consumidor final e o desperdício de
alimentos ocorrido durante esse período, que diminuirá a oferta
dessas mercadorias, aumentarão o seu preço. Além disso, não houve
redução no preço de outros combustíveis, que continuam muito acima
do preço praticado em outros países.
